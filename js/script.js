'use strict';

let Arr = [1, 1, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
let Arr2 = [10, 20, 30, 40, 50, 60, 70, 80, 90, 100];
let Arr3 = [
	{name: 'Marianna', experience: 7},
	{name: 'Karen', experience: 3},
	{name: 'Rubo', experience: 4},
	{name: 'Norayr', experience: 5},
	{name: 'Narek', experience: 2},
];
let NewArr = [];

// var - visible out of it's brackets
// let - visible only inside it's brackets
// const - impossible to change constant but possible change items of constant (in case of array)
/*for (var i = 0; i < 10; i++) {
	console.log(i);
}
console.log(i);*/

/*const j = 50;
console.log(j++);*/

/*const object = {
	a: 1,
	b: 2,
	c: 3
};

object.d = 10;
delete object['a'];
console.log(object);*/
// ________________________________________________________________________

// initial value for function parameter can be assigned to parameter directly
/*function f(par=10) {
	// par = par || 10;
	console.log(par);
}
f();*/
// ________________________________________________________________________

// operators
/*console.log(5 ** 2);

console.log(Number.isInteger(10));
console.log(Number.isInteger(10.5));

console.log(isFinite(10/0));
console.log(isFinite(10/1));

console.log(isNaN(100));
console.log(isNaN("text"));*/
// ________________________________________________________________________

// spread operator
/*console.log([1, 2, ...Arr, ...Arr2, 3, 4]);

let x = [...Arr];
// let x = Arr.slice();
// Arr[0] = 100;
console.log(x);

console.log(Math.max(...Arr));

const str = "Hello";
console.log([...str]);

function test(...args) {
	console.log(args)
}
test(1, 2, 3, 4, 5, 6, 7);*/
// ________________________________________________________________________

// in - index, of - value
/*for (let elem in Arr2) {
	console.log(elem);
}
for (let elem of Arr2) {
	console.log(elem);
}*/
// ________________________________________________________________________

// Arrow functions
/*function Timer(t) {
	this.time = t;

	this.tick = function() {
		this.time--;
		console.log(this.time);
	};

	// let that = this;
	// this.run = function() {
	// 	setInterval(function() {
	// 		that.tick();
	// 	}, 1000)
	// }

	this.run = () => {
		setInterval(() => {
			this.tick();
		}, 1000)
	}
}
let T = new Timer(100);
T.run();*/

/*let items = document.querySelectorAll('.item');

for (let item of items) {
	item.onclick = function() {
		console.log(this)
		this.classList.toggle('active');
	}
}*/
// ________________________________________________________________________

// rendering
/*let html = ''
for (let elem of Arr3) {
	html += '<div>' + elem['name'] + ' has ' + elem['experience'] + ' years experience in WEB development</div>';
	html += `<!--<div>${elem['name']} has ${elem['experience']} years experience in WEB development</div>-->`
}
document.querySelector('#block').innerHTML = html;*/
// ________________________________________________________________________

// Array.find - return value of first element if result is true;
// Array.findIndex - return index of first element if result is true;
/*let res = Arr.find((elem) => {
	if (elem === 2) {
		return true;
	}
});*/
// ________________________________________________________________________

// Array.map - returned value pushed to a new Array
/*NewArr = Arr.map((elem, index, array) => {
	return elem * index;
});*/
// ________________________________________________________________________

// Arr.filter - given element pushed to new Array if result is true
/*NewArr = Arr.filter((elem, index, array) => {
	if (elem % 2) {
		return true;
	}
});*/
// ________________________________________________________________________

// Array.reduce - on each step accumulator=return
/*let res = Arr.reduce((accumulator, elem, index, array) => {
	return accumulator + elem;
});*/

// console.log(res);
// document.querySelector('#block').innerHTML = res;
// ________________________________________________________________________

//OOP
/*
class Timer {
	constructor(t = 100) {
		this.time = t;
	}

	tick() {
		this.time--;
	};

	run() {
		setInterval(() => {
			this.tick();
		}, 1000)
	}
}

class ConsoleTimer extends Timer {
	tick() {
		super.tick();
		console.log(this.time);
	}
}

let T = new ConsoleTimer(100);
T.run();*/
